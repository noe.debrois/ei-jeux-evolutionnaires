from RotTable import *
from Traj3D import *
import numpy as np
from GeneticAlgorithm import *
import pickle


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--filename", help="input filename of DNA sequence")
parser.parse_args()
args = parser.parse_args()

### HYPERPARAMETERS ###
num_indiduals = 150  # nombre d'individus
num_selection = 50  # nombre d'individus que l'on conserve lors d'une prochaine génération
num_chromosomes = 10  # nombre de chromosomes
num_genes = 2  # nombre de gènes par chromosomes
proba_mutation = 0.05  # probabilité qu'un individu mute
num_epochs = 300  # nombre maximum de génération
max_no_improvement = 100  # nombre max


def Fitness(individu, dna_seq):  # fonction positive que l'on cherche à minimiser
    distance_coef = 1
    scalar_coef = 0

    traj = Traj3D()
    traj.compute(dna_seq, individu.to_table())
    traj = traj.getTraj()

    A = np.array(traj[0][0:3])
    B = np.array(traj[1][0:3])
    Z = np.array(traj[-1][0:3])
    Y = np.array(traj[-2][0:3])
    vec1 = A - B
    vec2 = Z-Y
    s = 0
    dot = np.dot(vec1, vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2))
    if(dot > -0.8):
        s += 10000
    return np.linalg.norm(Z-A)*distance_coef + (np.dot(vec1, vec2) / (np.linalg.norm(vec1)*np.linalg.norm(vec2)) + 1)*scalar_coef + s


def Fitness2(individu, dna_seq):  # fonction positive que l'on cherche à minimiser
    distance_coef = 1
    scalar_coef = 0

    traj = Traj3D()
    traj.compute(dna_seq, individu.to_table())
    traj = traj.getTraj()

    A = np.array(traj[0][0:3])
    B = np.array(traj[1][0:3])
    Z = np.array(traj[-1][0:3])
    Y = np.array(traj[-2][0:3])
    vec1 = A - B
    vec2 = Z-Y
    s = 0
    dot = np.dot(vec1, vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2))
    if(dot > -0.8):
        s += 10000
    return np.linalg.norm(Z-B)*distance_coef + (np.dot(vec1, vec2) / (np.linalg.norm(vec1)*np.linalg.norm(vec2)) + 1)*scalar_coef + s


def fitness_summary(individu, dna_seq):
    traj = Traj3D()
    traj.compute(dna_seq, individu.to_table())
    traj = traj.getTraj()

    A = np.array(traj[0][0:3])
    B = np.array(traj[1][0:3])
    Z = np.array(traj[-1][0:3])
    Y = np.array(traj[-2][0:3])
    vec1 = A-B
    vec2 = Z-Y
    mean = 0
    p1 = np.array(traj[0][0:3])
    for i in range(1, len(traj)):
        p2 = np.array(traj[i][0:3])
        delta = p2 - p1
        mean += np.linalg.norm(delta)
        p1 = p2
    mean = mean/(len(traj)-1)
    print(
        f"Distance entre le début et l'arrivée = {np.linalg.norm(Z-A)} (Ordre de grandeur d'un pas : {mean}) | Produit scalaire normalisé : {np.dot(vec1, vec2) / (np.linalg.norm(vec1)*np.linalg.norm(vec2)) }")
    if(args.filename):
        n = args.filename.split('_')[1].split('.')[0]
        norm = np.linalg.norm(Z-A)
        dot = np.dot(vec1, vec2) / (np.linalg.norm(vec1)*np.linalg.norm(vec2))
        file_handler = open(
            f'./outputs/{n}/dist({norm - norm%0.01})-dot({dot - dot%0.01}).obj', 'wb')
        pickle.dump(fitness_summary, file_handler)


def main():

    rot_table = RotTable()
    traj = Traj3D()

    if args.filename:
        # Read file
        lineList = [line.rstrip('\n') for line in open(args.filename)]
        # Formatting
        seq = ''.join(lineList[1:])

    else:
        seq = "AAAGGATCTTCTTGAGATCCTTTTTTTCTGCGCGTAATCTGCTGCCAGTAAACGAAAAAACCGCCTGGGGAGGCGGTTTAGTCGAAGGTTAAGTCAG"

    Gen = Population(num_indiduals, num_chromosomes, num_genes, num_selection, proba_mutation,
                     lambda individual: Fitness2(individual, seq), num_epochs, max_no_improvement)
    best_individual = Gen.train()
    fitness_summary(best_individual, seq)
    traj.compute(seq, best_individual.to_table())

    if args.filename:
        traj.draw(args.filename+".png")
    else:
        traj.draw("sample.png")


if __name__ == "__main__":
    main()
