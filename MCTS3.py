###Ce programme est la version finale de MCTS###


#### IMPORT ##
from importlib.resources import path
from operator import inv
from copy import deepcopy
from Traj3D import *
from math import inf
from random import randint
from random import choice
from RotTable import *
import numpy as np
from tqdm import tqdm

### GESTION DU NOM DE FICHIER POUR MAIN() ###
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--filename", help="input filename of DNA sequence")
parser.parse_args()
args = parser.parse_args()

### PARAMETRES APPELS RECURSIFS ###
import sys 
sys.setrecursionlimit(10**5) 

class Arbre:
    c = math.sqrt(2)
    discretisation = 5
    best_path = None
    best_inv_fitness = -inf
    def __init__(self,taille,feuille,N=0,best=0,successeurs={},c=c,best_path=best_path, \
        discretisation=discretisation,best_inv_fitness=best_inv_fitness,premier_inv_fitness=0):
        """
        PARAMETRES DES INSTANCES DE LA CLASSE Arbre
        """
        # taille : la taille de l'arbre (à expliciter)
        # feuille : bool qui vaut True si le noeud est une feuille
        # N : le nombre de fois où cet arbre a été exploré
        # Somme : (à expliciter)
        # successeurs : dictionnaire des successeurs d'un noeud (vide quand ce noeud est une feuille)
        # c : constante de la formule UCB1
        # best_path : path de la racine à la feuille qui donne la meilleure fitness
        # discretisation : subdivisions de l'intervalle [-SD;SD]
        # best_inv_fitness : plus grande valeur de 1/fitness      
        self.taille = taille
        self.feuille = feuille
        self.N = N
        self.best = best
        self.successeurs = successeurs
        self.c = c
        self.best_path = best_path
        self.discretisation = discretisation
        self.best_inv_fitness = best_inv_fitness
    
    def best_successeur(self,path,arbre_racine):
        """
        ATTENTION : score_feuille est une valeur arbitraire qu'il conviendra d'ajuster
        La fonction est en trois parties :
        1) Initialisation de max_successeurs : tuple contenant la valeur et la liste ;
        2) MAJ de max_successeurs ;
        3) Tirage du successeur.
        """
        if len(self.successeurs) != self.discretisation:
            #prendre une feuille
            return choice(list(set([k for k in range(self.discretisation)]).difference(set(self.successeurs)))),False
        else:
            #prendre le fils de score maximum
            max_successeur = -inf,[]
            for succ in self.successeurs:
                score_succ = UCB(self,self.successeurs[succ],arbre_racine)
                if score_succ > max_successeur[0]:
                    max_successeur = score_succ,[succ]
                elif score_succ == max_successeur[0]:
                    max_successeur[1].append(succ)
            return choice(max_successeur[1]),True
    
    def inv_fitness(self,path,dna_seq):
        assert self.feuille # Vérifier qu'on est bien une feuille ! (pour calculer 1/fitness...)
        fin_path = [randint(0,self.discretisation-1) for _ in range(self.taille)]
        if len(path+fin_path) == 19:
            fin_path.append(randint(0,self.discretisation-1))
        # Création d'une fin de path aléatoire (fin_path) pour descendre jusque tout en bas de l'arbre !
        return calcul_inv_fitness(self.discretisation,path+fin_path,dna_seq),path+fin_path # Puis calcul de 1/fitness
        # à partir de la feuille obtenue en suivant la path aléatoire depuis le noeud accédé via le path
        # normal. En gros on suit le path, puis le path aléatoire, puis on tombe sur une feuille et on 
        # calcule l'inverse de sa fitness. C'est le principe du MCTS.

    def Selection_new_feuille(self,path,arbre_racine):
        if self.taille == 1:
            return self,path # !! Arbre est l'avant feuille, path est le chemin jusqu'à la feuille !!

    ### SI LA TAILLE != 1 ###
        k,est_successeur = self.best_successeur(path,arbre_racine)
        path.append(k) # MAJ du path

        if est_successeur: # Appel récursif !
            return self.successeurs[k].Selection_new_feuille(path,arbre_racine)
        else: # On est en bas, i.e, une feuille !
            return self,path # !! Arbre est l'avant feuille, path est le chemin jusqu'a la feuille !!
    
    def Expansion(self,k):
        self.feuille = False # Car on fait une expansion...
        suc = Arbre(self.taille-1,True,successeurs={}) # suc est un arbre de taille arbre.taille-1\
        # qui est pour le moment un simple noeud (feuille), sans successeur.
        self.successeurs[k] = suc # suc est un successeur d'arbre.
        '''print('CREATION D UN NOUVEAU NOEUD')
        print(suc)'''

    def back_propagate(self,arbre_racine,path_to_feuille,path_agrandi,inv_fitness):
        """
        Cette fonction remonte le total d'une feuille selon la règle du MCTS.
        -> cf vidéo pour ne pas se tromper.
        """
        if arbre_racine.best_inv_fitness < inv_fitness: # Si la meilleure 1/fitness de tout l'arbre
        # est moins bonne que 1/fitness de cette feuille obtenue en partie par descente aléatoire...
            arbre_racine.best_path = path_agrandi # update du meilleur path de tout l'arbre !
            arbre_racine.best_inv_fitness = inv_fitness
        ### REMONTEE DE 1/fitness DE CETTE FEUILLE OBTENUE EN PARTIE PAR DESCENTE ALEATOIRE ###
        self.best = max(self.best,inv_fitness)
        self.N += 1
        #print(arbre.N)
        if len(path_to_feuille)>0:
            k = path_to_feuille.pop(0)
            self.successeurs[k].back_propagate(arbre_racine,path_to_feuille,path_agrandi,inv_fitness)

def UCB(pere,successeur,arbre_racine):
    ### RETURN UCB1 = score = w / n + c*sqrt(ln(N)/n) ###
    # On a w = successeur.Best/arbre_racine.Best = meilleur-ds-la-branche/meilleur-tout-court in [0, 1] ;
    # Et : n = successeur.N (i.e. le nombre de fois où l'on a visité le noeud parent du successeur).
    '''print('le pere,le fils et son score')
    print(pere)
    print(successeur)
    print((successeur.Somme/successeur.N) + \
        successeur.c*np.sqrt(math.log(pere.N)/(successeur.N)))'''
    return (successeur.best/(arbre_racine.best_inv_fitness*successeur.N)) + \
        successeur.c*np.sqrt(math.log(pere.N)/(successeur.N))

def calcul_inv_fitness(discretisation,path,dna_seq):
    """
    Cette fonction renvoie l'inverse de la fitness d'une branche.
    """

    ### OBTENTION DE LA ROTTABLE POUR CALCULER LA FITNESS ###
    rot = RotTable()
    table = path_to_table(path,discretisation)
    traj = Traj3D()
    keys = list(rot.getRot_Table().keys())
    dict_table = {keys[k] : (table[k]) for k in range(len(keys))}
    rot.setRot_Table(dict_table)
    traj.compute(dna_seq,rot)
    traj = traj.getTraj()
    A = np.array(traj[0][0:3])
    B = np.array(traj[1][0:3])
    Z = np.array(traj[-1][0:3])
    Y = np.array(traj[-2][0:3])
    vec1 = A-B
    vec2 = Z-Y

    ### CALCUL DE LA FITNESS ###
    s = abs(np.linalg.norm(Z-A)) #+ np.dot(vec1, vec2)) - np.linalg.norm(vec1)*np.linalg.norm(vec2))
    return 1/8*(3000-s)-275
    ### FIN CALCUL DE LA FITNESS ###

def path_to_table(path,discretisation):
    rot = RotTable()
    originale = np.array([rot.getORIGINAL_ROT_TABLE()[k] for k in rot.getORIGINAL_ROT_TABLE()])
    table = np.array([rot.getRot_Table()[k] for k in rot.getRot_Table()]) # Seulement les trois 1ères
    # colonnes de la RotTable ! (sans les SD...)
    ### FIN OBTENTION
    ### MODIFICATION DE LA TABLE PERMETTANT DE CALCULER A POSTERIORI LA TRAJ3D ###
    # On prend en compte les symétries de la RotTable originale qui doivent être maintenues !
    for p in range(len(path)):
        if p == 0: # AA : TT
            table[0] = table[0] + np.array([-1+2*path[0]/(discretisation-1),-1+2*path[1]/(discretisation-1),0])*np.array([originale[0,3],originale[0,4],0])
            table[15] = table[0]
            table[15,2] = - table[15,2]
        elif p == 1:
            table[1] = table[1] + np.array([-1+2*path[2]/(discretisation-1),-1+2*path[3]/(discretisation-1),0])*np.array([originale[1,3],originale[1,4],0])
            table[11] = table[1]
            table[11,2] = - table[11,2]
        elif p == 2:
            table[2] = table[2] + np.array([-1+2*path[4]/(discretisation-1),-1+2*path[5]/(discretisation-1),0])*np.array([originale[2,3],originale[2,4],0])
            table[7] = table[2]
            table[7,2] = - table[7,2]
        elif p == 4:
            table[4] = table[4] + np.array([-1+2*path[8]/(discretisation-1),-1+2*path[9]/(discretisation-1),0])*np.array([originale[4,3],originale[4,4],0])
            table[14] = table[4]
            table[14,2] = - table[14,2]
        elif p == 5:
            table[5] = table[5] + np.array([-1+2*path[10]/(discretisation-1),-1+2*path[11]/(discretisation-1),0])*np.array([originale[5,3],originale[5,4],0])
            table[10] = table[5]
            table[10,2] = - table[10,2]
        elif p == 8:
            table[8] = table[8] + np.array([-1+2*path[14]/(discretisation-1),-1+2*path[15]/(discretisation-1),0])*np.array([originale[8,3],originale[8,4],0])
            table[13] = table[8]
            table[13,2] = - table[13,2]
        elif p == 3: # pas de gène correpondant
            table[3] = table[3] + np.array([-1+2*path[6]/(discretisation-1),-1+2*path[7]/(discretisation-1),0])*np.array([originale[3,3],originale[3,4],0])
        elif p == 6:
            table[6] = table[6] + np.array([-1+2*path[12]/(discretisation-1),-1+2*path[13]/(discretisation-1),0])*np.array([originale[6,3],originale[6,4],0])
        elif p == 9:
            table[9] = table[9] + np.array([-1+2*path[16]/(discretisation-1),-1+2*path[17]/(discretisation-1),0])*np.array([originale[9,3],originale[9,4],0])
        elif p == 12:
            table[12] = table[12] + np.array([-1+2*path[18]/(discretisation-1),-1+2*path[19]/(discretisation-1),0])*np.array([originale[12,3],originale[12,4],0])
    return table

def afficher_path(dna_seq,path,arbre_racine):
    rot = RotTable()
    table = path_to_table(path,arbre_racine.discretisation)
    traj = Traj3D()
    keys = list(rot.getRot_Table().keys())
    dict_table = {keys[k] : (table[k]) for k in range(len(keys))}
    rot.setRot_Table(dict_table)
    traj.compute(dna_seq,rot)
    traj.draw(args.filename)

def main(nb_iter):
    ### GESTION DE LA SEQUENCE D'ENTREE ###
    if args.filename: # écrire filename--plasmid_(1)8(0)k.fasta
        # Read file
        lineList = [line.rstrip('\n') for line in open(args.filename)]
        # Formatting
        dna_seq = ''.join(lineList[1:])
    else: # séquence d'entrée si l'on ne précise rien dans la console
        dna_seq = "AAAGGATCTTCTTGAGATCCTTTTTTTCTGCGCGTAATCTGCTGCCAGTAAACGAAAAAACCGCCTGGGGAGGCGGTTTAGTCGAAGGTTAAGTCAG"

    ### CREATION DE L'ARBRE D'ORIGINE ###
    arbre_racine = Arbre(taille=20,feuille=True) # instance de départ de la classe Arbre
    # On part d'un arbre de taille 15 (0->15 pour les 16 paramètres),\
    # qui est à l'origine une feuille puisque composé d'un seul noeud.

    for _ in tqdm(range(nb_iter)):
        '''print('Arbre racine et ses succ')
        print(arbre_racine)
        print(arbre_racine.successeurs)'''
        av_feuille,path_to_feuille = arbre_racine.Selection_new_feuille([],arbre_racine)
        '''print('avant feuille')
        print(av_feuille)
        print(av_feuille.successeurs)
        print('path_to_feuille')
        print(path_to_feuille)'''
        av_feuille.Expansion(path_to_feuille[-1]) # Expansion à partir de la feuille.
        feuille = av_feuille.successeurs[path_to_feuille[-1]] # la feuille est le successeur \
        # de l'avant dernier-noeud ; trouvée en choisissant la path_to_feuille[-1]-èime branche\
        # comme successeur de av_feuille.
        '''
        print('La feuille')
        print(feuille)
        '''
        inv_fit,path_agrandi = feuille.inv_fitness(path_to_feuille,dna_seq)
        '''print('inv_fit')
        print(inv_fit)
        print('path_agrandi')
        print(path_agrandi)'''
        # path_agrandi : path classique + descente aléatoire jusqu'au bas de l'arbre.
        # inv_fit : 1/fitness d'une feuille obtenu via path_agrandi !
        arbre_racine.back_propagate(arbre_racine,path_to_feuille,path_agrandi,inv_fit)
    return arbre_racine.best_path,800-8*arbre_racine.best_inv_fitness,dna_seq,arbre_racine


if __name__ == '__main__':
    s = main(10000)
    print(s[1],s[0])
    afficher_path(s[2],s[0],s[3])
