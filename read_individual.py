import argparse
from RotTable import *
from Traj3D import *
import numpy as np
from GeneticAlgorithm import *
import pickle

parser = argparse.ArgumentParser()
parser.add_argument("--filename", help="input filename of DNA sequence")
parser.add_argument("--seq")
parser.parse_args()
args = parser.parse_args()

if(not args.filename or not args.seq):
    print("Il manque les argumenrs --filename et --seq")
else:
    # Read file
    lineList = [line.rstrip('\n') for line in open(f"./plasmid_{args.seq}.fasta")]
    # Formatting
    seq = ''.join(lineList[1:])
    # Open the file in read mode
    objectRep = open(f"./outputs/{args.seq}/{args.filename}.obj", "rb")
    print("Unpickling...")

    # Unpickle the objects
    rot_table = RotTable()
    traj = Traj3D()
    gen = pickle.load(objectRep)
    traj.compute(seq, gen)
    print("Computation done")
    traj.draw(args.seq+".png")
    



    
        
