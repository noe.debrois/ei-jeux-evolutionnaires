import os
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("--seq", help="input filename of DNA sequence")
parser.parse_args()
args = parser.parse_args()

path =f'outputs\{args.seq}'
list_of_files = []

if(not args.seq):
    print("Utiliser l'argument --seq")

for root, dirs, files in os.walk(path):
	for file in files:
		list_of_files.append(os.path.join(root,file))

X = []
Di = []
Do = []
fit = []
dicFit = [[] for i in range(500)]

for filename in list_of_files:
    
    ind = int(filename.split("ind(")[1].split(")")[0])
    dist = float(filename.split("dist(")[1].split(")")[0])
    dot = float(filename.split("dot(")[1].split(")")[0])
    dicFit[ind].append(dist)

    X.append(ind)
    Di.append(dist)
    Do.append(Do)
    fit.append(dist + 59*(dot+1))

print(f"Number of points : {len(list_of_files)}")
a = plt.plot(X,fit,'o')
plt.yscale("log")

plt.xlabel("Population")
plt.ylabel("Fitness")
ax = plt.gca()

plt.show()
