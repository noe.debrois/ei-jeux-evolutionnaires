# EI jeux évolutionnaires

# Comment utiliser nos algorithmes ?

installer tqdm:
pip install tqdm

# Pour lancer l'algorithme génétique:
py ./Main.py --filename nomdufichierdelasequence

#Changement des méthodes de sélection/mutation pour l'algorithme génétique
remplacer mutation par mutation_par_gene ligne 109 du fichier GeneticAlgorithm.py 
remplacer selection par alternative_selection ligne 107 du même fichier

# Pour lancer l'algorithme de MonteCarlo:
py ./MCTS3.py --filename nomdufichierdelasequence


# Affichage des solutions

Deux programmes permettent d'afficher des solutions : 

plot_results.py : Affiche la repartition des fitness en fonction de la population

Pour le lancer:
py .\plot_results.py --seq 8k_ou_180k

Exemple : 
py .\plot_results.py --seq 8k

read_individual.py : Affiche un individu enregistré

Pour le lancer :
py .\read_individual.py --seq 8k_ou_180k --filename "nom_du_fichier_sans_le_.obj"

Exemple :
py .\read_individual.py --seq 8k --filename "ind(200)-dist(0.22)-dot(-1.0)"
