from GeneticAlgorithm import *
from Main import Fitness


class Test_population:

    def test_selection(self):
        population = Population(num_individuals=4, num_chromosomes=5, num_genes=2, num_selection=1,
                                proba_mutation=1, fitness=lambda: 2, num_epochs=10, max_no_improvemement=5)
        individual1 = Individual(
            num_chromosomes=5, num_genes=2, fitness=lambda: 2)
        individual2 = Individual(
            num_chromosomes=5, num_genes=2, fitness=lambda: 2)
        individual1.score = 1
        individual2.score = 2
        population.population = [individual1, individual2]
        poptest = deepcopy(population)
        population.selection()

        if poptest == population:
            print("implode")
        # on vérifie que l'on a sélectionné le bon individu
        assert population.population[0].score == individual1.score

    def test_crossover(self):
        population = Population(num_individuals=4, num_chromosomes=5, num_genes=2, num_selection=1,
                                proba_mutation=1, fitness=lambda: 2, num_epochs=10, max_no_improvemement=5)
        individual1 = Individual(
            num_chromosomes=5, num_genes=2, fitness=lambda: 2)
        individual2 = Individual(
            num_chromosomes=5, num_genes=2, fitness=lambda: 2)
        gene1 = Gene()
        gene2 = Gene()

        gene1.value = -0.2
        gene2.value = 0.2
        for i in range(5):
            for j in range(2):
                individual1.chromosomes[i].genes[j] = gene1
                individual2.chromosomes[i].genes[j] = gene2
        population.population = [
            deepcopy(individual1), deepcopy(individual2)]
        population.crossover()
        L = [[[-0.2, -0.2]]*k+[[0.2, 0.2]]*(5-k) for k in range(1, 5)] + [
            [[0.2, 0.2]]*k+[[-0.2, -0.2]]*(5-k) for k in range(1, 5)]  # forme possible des individus après croisements

        # on vérifie que l'individu croisé a une forme acceptable
        assert population.population[2].get_value() in L

    def test_mutation(self):
        population = Population(num_individuals=2, num_chromosomes=5, num_genes=2, num_selection=1,
                                proba_mutation=1, fitness=lambda: 2, num_epochs=10, max_no_improvemement=5)
        pop2 = deepcopy(population)
        pop2.mutation_par_gene()
        pop3 = deepcopy(population)
        pop3.mutation
        # on vérifie que la population a bien été modifiée (la probabilité de mutation est de 1 donc il y a nécessairement des changements)
        assert pop3 != population
        assert pop2 != population


class Test_individual():
    individual = Individual(10, 2, Fitness)
    gene = Gene()
    gene.value = 0.2
    for i in range(10):
        for j in range(2):
            individual.chromosomes[i].genes[j] = deepcopy(gene)

    def test_get_value(self):
        assert self.individual.get_value() == [[0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [
            0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2]]

    def test_to_table(self):
        assert self.individual.to_table().getRot_Table() == {
            "AA": [35.62+0.012, 7.2+0.12, -154],
            "AC": [34.4+0.26, 1.1+1,  143],
            "AG": [27.7+0.3, 8.4+0.6,    2],
            "AT": [31.5+0.22, 2.6+0.4,    0],
            "CA": [34.5+0.18, 3.5+34*0.2,  -64],
            "CC": [33.67+0.2*0.07, 2.1+0.2*2.1,  -57],
            "CG": [29.8+1.1*0.2, 6.7+0.2*1.5,    0],
            "CT": [27.7+0.3, 8.4+0.6,   -2],
            "GA": [36.9+0.9*0.2, 5.3+0.2*6,  120],
            "GC": [40+0.2*1.2, 5+0.2*1.275,  180],
            "GG": [33.67+0.07*0.2, 2.1+2.1*0.2,   57],
            "GT": [34.4+0.26, 1.1+1, -143],
            "TA": [36+1.1*0.2, 0.9+0.2*2,    0],
            "TC": [36.9+0.9*0.2, 5.3+0.2*6, -120],
            "TG": [34.5+0.18, 3.5+34*0.2,   64],
            "TT": [35.62+0.012, 7.2+0.12,  154]}  # valeur theorique de la table calculée à la main

    def test_mutate(self):
        self.ind2 = deepcopy(self.individual)
        self.ind2.mutate()
        assert self.individual != self.ind2  # on vérifie que l'individu a bien muté


class Test_chromosome:
    chromosome = Chromosome(2)
    gene = Gene()
    gene.value = 0.2
    chromosome.genes = [gene, gene]

    def test_get_value(self):
        assert self.chromosome.get_value() == [0.2, 0.2]

    def test_mutate(self):
        chr2 = deepcopy(self.chromosome)
        chr2.mutate_chromosome()
        assert chr2 != self.chromosome  # on vérifie que le chromosome a bien muté
