import random
import numpy as np
from RotTable import RotTable
from Traj3D import *
from copy import deepcopy


class Population:  # Défintion de la population

    def __init__(self, num_individuals, num_chromosomes, num_genes, num_selection, proba_mutation, fitness, num_epochs, max_no_improvemement):
        self.num_individuals = num_individuals  # Nombre d'individu dans la population
        self.proba_mutation = proba_mutation  # Probabilité qu'un individu mute
        self.fitness = fitness  # Fitness de l'algorithme
        self.num_chromosomes = num_chromosomes  # Nombre de chromosome par individu
        self.num_genes = num_genes  # Nombre de genes par chromosome
        self.num_selection = num_selection  # Nombre d'individu selection
        self.num_epochs = num_epochs  # Nombre d'epochs
        self.num_first_individuals = 10 * num_individuals
        # Nombre maximum d'epochs sans améliorations (Si il est atteint, l'entrainement s'arrête)
        self.max_no_improvement = max_no_improvemement

        self.population = [Individual(num_chromosomes, num_genes, fitness)  # On instancie la population
                           for _ in range(self.num_first_individuals)]

    def selection(self):  # Fonction correspondant à l'étape de selection (selection par tournoi)
        new_pop = []
        for _ in range(self.num_selection):
            combattants = np.random.choice(
                self.population, size=2, replace=False)
            # On cherche à obtenir la fitness la plus petite possible

            if combattants[0].score < combattants[1].score:
                new_pop.append(combattants[0])
            else:
                new_pop.append(combattants[1])
        self.population = new_pop  # On remplace l'ancienne population par la nouvelle

    def alternative_selection(self):
        L = [1/individual.score for individual in self.population]
        L = L/sum(L)
        new_pop = list(np.random.choice(self.population,
                                        size=self.num_selection, replace=False, p=L))
        self.population = deepcopy(new_pop)

    def crossover(self):  # Fonction de croisement
        new_pop = deepcopy(self.population)
        for _ in range((self.num_individuals-len(self.population))//2):
            crossed_individuals = np.random.choice(  # On choisit 2 individus
                self.population, size=2, replace=False)
            n = random.randint(1, len(crossed_individuals[0].chromosomes)-2)
            new_individual1 = Individual(  # On instancie les deux enfants
                self.num_chromosomes, self.num_genes, self.fitness)
            new_individual2 = Individual(
                self.num_chromosomes, self.num_genes, self.fitness)

            new_individual1.chromosomes = deepcopy(
                crossed_individuals[0].chromosomes[:n] + crossed_individuals[1].chromosomes[n:])  # On leur affecte leurs nouveaux chromosomes
            new_individual2.chromosomes = deepcopy(
                crossed_individuals[1].chromosomes[:n] + crossed_individuals[0].chromosomes[n:])
            # On ajoute les deux nouveaux individus dans la population
            new_pop.append(new_individual1)
            new_pop.append(new_individual2)
        self.population = new_pop

    def mutation(self):  # Fonction mutation : Chaque individu a une probabilité proba_mutation de muter
        for individual in self.population:
            if(random.random() < self.proba_mutation):
                individual.mutate()

    # deuxième méthode de mutation : on mute les gènes avec une probabilité de mutation plus faible
    def mutation_par_gene(self):
        for ind in self.population:
            ind.mutation_genes(self.proba_mutation)

    def evaluate(self):  # Recalcule la fitness de chaque individu
        for individual in self.population:
            individual.evaluate()

    def train(self):  # Entrainement de la population
        no_improvement = 0  # Initialisation de la variable d'earlystopping
        best_of_the_better = None  # Meilleur des meilleurs
        for i in range(self.num_epochs):
            print(f"----------------- EPOCH {i+1} -----------------")
            self.evaluate()  # On calcule la fitness de chaque individus
            best_individual = self.get_best()

            if(i == 0):
                best_of_the_better = deepcopy(self.population[0])

            # Si le meilleur n'a jamais été assigné, on choisit le premier de la population

            # Si il n'y a pas d'ameliorations, on incrémente la variable d'earlystopping

            # Sinon, on a une amélioration : on remplace le meilleur individu de l'entrainement
            if(best_individual.score < best_of_the_better.score):
                best_of_the_better = deepcopy(best_individual)
                no_improvement = 0
                print(f" Max score : {best_of_the_better.score}")
            else:
                print(f" Max score : {best_of_the_better.score}")
                no_improvement += 1
                print(
                    f" No improvement for {no_improvement}/{self.max_no_improvement}")

            # Étapes de l'entrainement:

            self.selection()  # Selection
            self.crossover()  # Croisement
            self.mutation_par_gene()  # Mutation

            self.population = self.population[:self.num_individuals-1]+[
                deepcopy(best_of_the_better)]  # On conserve le meilleur à travers les générations

            #print(self.taux_homogénéité())

            if(no_improvement == self.max_no_improvement):
                print(
                    f"No improvement for {self.max_no_improvement} : Training terminated")
                break
        return best_of_the_better

    def get_best(self):  # renvoie le meilleur individu de la population
        score = math.inf
        mem = None
        for individual in self.population:
            if individual.score < score:

                mem = individual
                score = individual.score

        return mem

    def taux_homogénéité(self):# renvoie une liste avec le nombre d'individus ayant la même fitness pour chaque valeur
        dict = {}
        mean = 0
        for individual in self.population:
            if not hasattr(individual, 'score'):
                individual.evaluate()
            if individual.score in dict:
                dict[individual.score] += 1
            else:
                dict[individual.score] = 1
            mean += individual.score
        #print(mean/self.num_individuals)
        return dict.values()


class Individual:
    gene_to_int = {'AA': 0, 'AC': 1, 'CA': 2, 'AG': 3,
                   'AT': 4, 'TA': 5, 'CC': 6, 'GA': 7, 'GC': 8, 'CG': 9}  # on attribue aux dinucélotides une valeur
    corresponding_gene = {'TT': 'AA', 'GT': 'AC',
                          'TG': 'CA', 'CT': 'AG', 'GG': 'CC', 'TC': 'GA'}  # certains dinucléotides sont liés dans la table (twist égaux, wedge égaux et directory opposés)

    def __init__(self, num_chromosomes, num_genes, fitness):
        self.num_chromosomes = num_chromosomes
        self.num_genes = num_genes

        self.chromosomes = [Chromosome(num_genes)
                            for _ in range(self.num_chromosomes)]

        self.fitness = fitness

    def get_value(self):  # renvoie une matrice avec les valeurs des gènes de l'individu
        return [chromosome.get_value() for chromosome in self.chromosomes]

    def to_table(self):  # renvoie un élément de la classe RotTable à partir d'un individu
        rot_table = RotTable()
        table = rot_table.getRot_Table()
        original_rot_table = rot_table.getORIGINAL_ROT_TABLE()

        for i in table:  # pour chaque dinucléotide
            if i in self.corresponding_gene:  # on ne traite pas les dinucléotides correspondant deux fois
                dinucleotide = self.corresponding_gene[i]
            else:
                dinucleotide = i
            for j in range(len(table[i])):
                if j == 2:
                    if dinucleotide != i:
                        table[i][j] = - \
                            original_rot_table[dinucleotide][2]
                    else:
                        table[dinucleotide][j] = original_rot_table[dinucleotide][2]
                else:

                    # modification de la table en utilisant les incertitudes contenues dans les gènes de l'individu
                    table[i][j] = table[i][j] + self.get_value(
                    )[self.gene_to_int[dinucleotide]][j] * original_rot_table[dinucleotide][j+3]

        RotTable.setRot_Table(rot_table, table)
        return rot_table

    def evaluate(self):  # la fitness d'un individu est l'attribut score
        self.score = self.fitness(self)

    def mutate(self):  # si un individu mute, un de ses chromosomes mute
        n = random.randint(0, self.num_chromosomes - 1)
        self.chromosomes[n].mutate_chromosome()

    def mutation_genes(self, proba_mutation):
        for chr in self.chromosomes:
            chr.mutation_genes(proba_mutation)


class Chromosome:
    # Chromosome: triplet de gènes. 1er gène=écart de valeur de twist; 2ème: écart sur celle de Wedge, et le 3ème à la direction.
    def __init__(self, num_genes):

        self.num_genes = num_genes
        # initialisation du chromosome
        self.genes = [Gene() for _ in range(num_genes)]

    # Si un chromosome mute, on choisit aléatoirement le gène mutant
    def mutate_chromosome(self):
        i = random.randint(0, self.num_genes - 1)

        # on appelle la focntion faisant muter les gènes
        self.genes[i].mutate_gene()

    def get_value(self):  # renvoie une liste contenant les valeurs des gènes
        return [gene.value for gene in self.genes]

    def mutation_genes(self, proba_mutation):
        for gene in self.genes:
            gene.mutation_genes(proba_mutation)


class Gene:  # Gene correspond aux gènes de l'algorithme: chaque gène contient une valeur réelle entre -1 et 1
    def __init__(self):
        # on inititalise avec une valeur aléatoire
        self.value = random.uniform(-1, 1)

    def mutate_gene(self):  # on mute un gène: on réinitialise la valeur
        self.value = random.uniform(-1, 1)

    def get_value(self):  # donne la valeur correspondant au gène
        return self.value

    def mutation_genes(self, proba_mutation):
        if random.random() <= proba_mutation:
            self.value = random.uniform(-1, 1)
