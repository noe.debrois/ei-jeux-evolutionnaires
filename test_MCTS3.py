from MCTS2 import Selection_new_feuille
from MCTS3 import *
import numpy as np

##TEST DU PROGRAMME MCTS3#
def test_best_succ():
    arbre1 = Arbre(1,False,N=3,best=10,successeurs={
        0 : Arbre(0,True,N=1,best=10),
        1 : Arbre(0,True,N=1,best=0)
    },discretisation=2,best_inv_fitness=10)
    arbre2 = Arbre(1,False,N=7,successeurs={
        0 : Arbre(0,True,N=1,best=10),
        1 : Arbre(0,True,N=5,best=11)
    },discretisation=2,best_inv_fitness=11)
    arbre3 = Arbre(1,False,N=32,successeurs={
        0 : Arbre(0,True,N=1,best=10),
        1 : Arbre(0,True,N=30,best=11)
    },discretisation=3,best_inv_fitness=11)
    assert arbre1.best_successeur([],arbre1)==(0,True)
    assert arbre2.best_successeur([],arbre2)==(0,True)
    assert arbre3.best_successeur([],arbre3)==(2,False)

test_best_succ()

def test_path_to_table():
    path = [6 for i in range(20)]
    table = path_to_table(path,11)
    
    valide_table =  np.array([[35.62+0.012, 7.2+0.12, -154],
        [34.4+0.26, 1.1+1,  143],
        [27.7+0.3, 8.4+0.6,    2],
        [31.5+0.22, 2.6+0.4,    0],
        [34.5+0.18, 3.5+34*0.2,  -64],
        [33.67+0.2*0.07, 2.1+0.2*2.1,  -57],
        [29.8+1.1*0.2, 6.7+0.2*1.5,    0],
        [27.7+0.3, 8.4+0.6,   -2],
        [36.9+0.9*0.2, 5.3+0.2*6,  120],
        [40+0.2*1.2, 5+0.2*1.275,  180],
        [33.67+0.07*0.2, 2.1+2.1*0.2,   57],
        [34.4+0.26, 1.1+1, -143],
        [36+1.1*0.2, 0.9+0.2*2,    0],
        [36.9+0.9*0.2, 5.3+0.2*6, -120],
        [34.5+0.18, 3.5+34*0.2,   64],
        [35.62+0.012, 7.2+0.12,  154]])
    
    difference = abs(valide_table - table)
    shape = difference.shape
    for i in range(shape[0]):
        for j in range(shape[1]):
            assert difference[i,j] < 10**-14

def test_back_propagate():
    arbre1 = Arbre(1,False,N=3,successeurs={
        0 : Arbre(0,False,N=1,best=2,successeurs={0:Arbre(0,True,N=1,best = 10)}),
        1 : Arbre(0,True,N=1,best=5)
    },discretisation=2)
    arbre1.back_propagate(arbre1,[0,0],[0,0],10)
    assert arbre1.best == 10
    assert arbre1.successeurs[0].best == 10
    assert arbre1.successeurs[1].best == 5




