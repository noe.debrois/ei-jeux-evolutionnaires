import sys
from operator import inv
from Traj3D import *
from math import inf
from random import randint
from random import choice
from RotTable import *
import numpy as np
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--filename", help="input filename of DNA sequence")
parser.parse_args()
args = parser.parse_args()
sys.setrecursionlimit(10**5)


class Arbre:
    c = math.sqrt(2)
    clee = [k for k in range(16)]
    discretisation = 50
    best_path = None

    def __init__(self, taille, feuille, N=0, Best=0, successeurs={}, c=c, best_path=best_path, discretisation=discretisation):
        # N : le nombre de fois où cet arbre a été exploré
        # Best : score maximum des fils
        # Somme : somme des scores des fils
        # successeurs : vide quand on a une feuille
        # taille : la taille de l'arbre
        self.best_path = best_path
        self.c = c
        self.discretisation = discretisation
        self.feuille = feuille
        self.taille = taille
        self.N = N
        self.Best = Best
        self.successeurs = successeurs

    def best_successeur(self, path, dna_seq, arbre_racine, score_feuille=inf):
        # score_feuille est une valeur arbitraire qu'il conviendra d'ajuster
        # la liste contient les gens qui ont le best score
        max_successeurs = (-inf, [])
        # on initialise à -inf car on travaille désormais avec 1/fitness
        for nb_successeur in self.successeurs:
            successeur = self.successeurs[nb_successeur]
            if score(self, successeur, arbre_racine) > max_successeurs[0]:
                max_successeurs = score(self, successeur, arbre_racine), [
                    nb_successeur]
            elif score(self, successeur, arbre_racine) == max_successeurs[0]:
                max_successeurs[1].append(nb_successeur)
        if max_successeurs[0] > score_feuille or len(self.successeurs) == self.discretisation:
            # on choisit un des gens qui ont le best score, le booleen indique qu'il est dans les successeurs
            return choice(max_successeurs[1]), True
        elif max_successeurs[0] == score_feuille:
            E1 = set(max_successeurs[1])
            E2 = set([k for k in range(self.discretisation)])
            E3 = set(self.successeurs)
            k = choice(list(E1.union(E2.difference(E3))))
            if k in E1:
                return k, True  # le booleen indique s'il est dans les successeurs
            else:
                return k, False
        else:
            E2 = set([k for k in range(self.discretisation)])
            E3 = set(self.successeurs)
            return choice(list(E2.difference(E3))), False


def score(pere, successeur, arbre_racine):
    # print(successeur.Somme/(successeur.Best*successeur.N) + \
    # successeur.c*np.sqrt(math.log(pere.N)/(successeur.Best*successeur.N)))
    return successeur.Best/(arbre_racine.Best*successeur.N) + \
        successeur.c*np.sqrt(math.log(pere.N)/(successeur.N)
                             )  # tout est regroupé dans la cte c


def calcul_inv_fitness(feuille, path, dna_seq):
    rot = RotTable()
    originale = np.array([rot.getORIGINAL_ROT_TABLE()[k]
                          for k in rot.getORIGINAL_ROT_TABLE()])
    table = np.array([rot.getRot_Table()[k] for k in rot.getRot_Table()])
    for p in range(len(path)):
        k = feuille.clee[p]
        if k == 0:  # AA : TT
            table[0] = table[0] + (-1+2*path[0]/feuille.discretisation) * \
                np.array([originale[0, 3], originale[0, 4], 0])
            table[15] = table[0]
            table[15, 2] = - table[15, 2]
        elif k == 1:
            table[1] = table[1] + (-1+2*path[1]/feuille.discretisation) * \
                np.array([originale[1, 3], originale[1, 4], 0])
            table[11] = table[1]
            table[11, 2] = - table[11, 2]
        elif k == 2:
            table[2] = table[2] + (-1+2*path[2]/feuille.discretisation) * \
                np.array([originale[2, 3], originale[2, 4], 0])
            table[7] = table[2]
            table[7, 2] = - table[7, 2]
        elif k == 4:
            table[4] = table[4] + (-1+2*path[4]/feuille.discretisation) * \
                np.array([originale[4, 3], originale[4, 4], 0])
            table[14] = table[4]
            table[14, 2] = - table[14, 2]
        elif k == 5:
            table[5] = table[5] + (-1+2*path[5]/feuille.discretisation) * \
                np.array([originale[5, 3], originale[5, 4], 0])
            table[10] = table[5]
            table[10, 2] = - table[10, 2]
        elif k == 8:
            table[8] = table[8] + (-1+2*path[8]/feuille.discretisation) * \
                np.array([originale[8, 3], originale[8, 4], 0])
            table[13] = table[8]
            table[13, 2] = - table[13, 2]
        elif k in [3, 6, 9, 12]:  # pas de gène correpondant
            table[k] = table[k] + (-1+2*path[k]/feuille.discretisation) * \
                np.array([originale[k, 3], originale[k, 4], 0])
    traj = Traj3D()
    keys = list(rot.getRot_Table().keys())
    dict_table = {keys[k]: (table[k]) for k in range(len(keys))}
    rot.setRot_Table(dict_table)
    traj.compute(dna_seq, rot)
    traj = traj.getTraj()
    A = np.array(traj[0][0:3])
    B = np.array(traj[1][0:3])
    Z = np.array(traj[-1][0:3])
    Y = np.array(traj[-2][0:3])
    vec1 = A-B
    vec2 = Z-Y
    # - np.linalg.norm(vec1)*np.linalg.norm(vec2))
    s = abs(np.linalg.norm(Z-A) + np.dot(vec1, vec2))
    if s == 0:
        return inf
    else:
        return 1/s


"""
def inv_fitness(feuille,path,dna_seq):
    # path : liste des indices des successeurs choisis
    assert feuille.feuille
    if feuille.taille == 0: # quand on est une feuille : condition d'arrêt
        return calcul_inv_fitness(feuille,path,dna_seq),path # calcul de l'inverse de la finesse
    r = choice(0,feuille.discretisation) # tir au hasard du numéro du successeur

    path.append(r) # choix au hasard du successeur
    inv_fitness(feuille.successeurs[r],path) # appel récursif tant qu'on n'est pas en bas de l'arbre !
"""


def inv_fitness(feuille, path, dna_seq):
    assert feuille.feuille
    fin_path = [randint(0, feuille.discretisation)
                for _ in range(feuille.taille)]
    return calcul_inv_fitness(feuille, path+fin_path, dna_seq), path+fin_path


def Selection_new_feuille(arbre, path, dna_seq, arbre_racine):
    if arbre.taille == 1:
        return arbre, path
    k, est_successeur = arbre.best_successeur(path, dna_seq, arbre_racine)
    path.append(k)
    if est_successeur:
        return Selection_new_feuille(arbre.successeurs[k], path, dna_seq, arbre_racine)
    else:
        # ATTENTION Arbre est l'avant feuille, path est le chemin jusqu'a la feuille
        return arbre, path


def Expansion(arbre, k):
    arbre.feuille = False
    suc = Arbre(arbre.taille-1, True, successeurs={})
    arbre.successeurs[k] = suc
    # print(arbre.taille-1)


def back_propagate(arbre, est_racine, path_to_feuille, path_agrandi, inv_fitness):
    if est_racine and arbre.Best < inv_fitness:
        arbre.best_path = path_agrandi
    arbre.Best = max(arbre.Best, inv_fitness)
    arbre.N += 1
    # print(arbre.N)
    if len(path_to_feuille) > 0:
        k = path_to_feuille.pop(0)
        back_propagate(arbre.successeurs[k], False,
                       path_to_feuille, path_agrandi, inv_fitness)


def main(nb_iter):

    if args.filename:
        # Read file
        lineList = [line.rstrip('\n') for line in open(args.filename)]
        # Formatting
        dna_seq = ''.join(lineList[1:])

    else:
        dna_seq = "AAAGGATCTTCTTGAGATCCTTTTTTTCTGCGCGTAATCTGCTGCCAGTAAACGAAAAAACCGCCTGGGGAGGCGGTTTAGTCGAAGGTTAAGTCAG"

    arbre_racine = Arbre(taille=15, feuille=True)
    for _ in range(nb_iter):
        av_feuille, path_to_feuille = Selection_new_feuille(
            arbre_racine, [], dna_seq, arbre_racine)
        Expansion(av_feuille, path_to_feuille[-1])
        feuille = av_feuille.successeurs[path_to_feuille[-1]]
        inv_fit, path_agrandi = inv_fitness(feuille, path_to_feuille, dna_seq)
        back_propagate(arbre_racine, True, path_to_feuille,
                       path_agrandi, inv_fit)
    return arbre_racine.best_path, 1/arbre_racine.Best


if __name__ == '__main__':
    print(main(10000))
